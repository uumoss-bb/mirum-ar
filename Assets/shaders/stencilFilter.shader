﻿Shader "Custom/stencilFilter" {

	Properties {

		_Color ("Color", Color) = (1,1,1,1)
		[Enum(Equal,3,NotEqual,6)] _StecilTest ("Stencil Test", int) = 3
	}

	SubShader {

		Color [_Color]

	//This says "hey we will sender if we see 1's in the stencil buffer"
	// So any obj that is being looked through another obj that the pixels
	// are set to 1 in the stencil buffer
		Stencil {
			Ref 1
			Comp [_StecilTest]
		}

		Pass {
			
		}
	}
}
