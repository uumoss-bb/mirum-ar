// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Diffuse shader. Differences from regular Diffuse one:
// - no Main Color
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/portalFilter" {
Properties {
    _MainTex ("Base (RGB)", 2D) = "white" {}

    //brodie put this here
    [Enum(Equal,3,NotEqual,6)] _StecilTest ("Stencil Test", int) = 3
}
SubShader {

	//This says "hey we will sender if we see 1's in the stencil buffer"
	// So any obj that is being looked through another obj that the pixels
	// are set to 1 in the stencil buffer
		Stencil {
			Ref 1
			Comp [_StecilTest]
		}


    Tags { "RenderType"="Opaque" }
    LOD 150

CGPROGRAM
#pragma surface surf Lambert noforwardadd

sampler2D _MainTex;

struct Input {
    float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) {
    fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
    o.Albedo = c.rgb;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Mobile/VertexLit"
}
