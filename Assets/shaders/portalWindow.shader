﻿Shader "Custom/portalWindowShader" {

	SubShader {
	// this makes the object transparent
		ZWrite off
		colorMask 0
		Cull off

	//This sets each pixel to 1 in the stencil buffer
		Stencil {
			Ref 1
			Pass replace
		}

		Pass {
	
		}
	}
}
