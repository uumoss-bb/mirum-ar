﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		transform.Translate(Input.GetAxis("Horizontal"),0, Input.GetAxis("Vertical"));

		float rotation = 0;
		if (Input.GetKey(KeyCode.Q)) {
			rotation -= 1;
		}
		if (Input.GetKey (KeyCode.E)) {
			rotation += 1;
		}
		float rotationVertical = 0;
		if (Input.GetKey(KeyCode.T)) {
			rotationVertical -= 1;
		}
		if (Input.GetKey (KeyCode.R)) {
			rotationVertical += 1;
		}

		transform.Rotate (rotationVertical, rotation, 0);
	}
}
