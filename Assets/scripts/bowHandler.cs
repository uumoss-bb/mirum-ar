﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bowHandler : MonoBehaviour {
	
	public Material bowMaterial;
	public Transform player;
	public GameObject anArrow;
	public Transform arrowStorage;
	public float NextShot = 0.0f;
	public float brX = 0.0f;
	public float brY = 0.0f;
	public float brZ = 0.0f;
	public float bpZ = 1.2f;
	public float bpX = 0.1f;
	public float bpY = 0.1f;

	public float arX = 0.0f;
	public float arY = 0.0f;
	public float arZ = 0.0f;
	public float apZ = 1.2f;
	public float apX = 0.1f;
	public float apY = 0.1f;

	public static bool bowInHand;
	float timeTillNextShot = 0.0f;
	float velocity = 80f;
	BoxCollider bc;

	// Use this for initialization
	void Start () {

		bc = GetComponent<BoxCollider> ();
	}

	void OnTriggerEnter (Collider other) {

		if(other.transform == player) {
			
			bowInHand = true;
			return;
		}

		return;
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		var stencilTest = bowMaterial.GetInt ("_StecilTest");

		//If it is not rendering we dont want to pick it up
		if (stencilTest == 6) { 
			bc.isTrigger = true;
		}

		if (bowInHand) {

			GameObject newArrow;
			Vector3 arrowPosition = player.transform.position + player.transform.forward * apZ + player.right * apX + player.up * apY;
			Quaternion arrowRotation = Quaternion.Euler(player.rotation.eulerAngles.x + arX, player.rotation.eulerAngles.y + arY, player.rotation.eulerAngles.z + arZ);

			//this sets the bow in hand.
			transform.rotation = Quaternion.Euler(player.rotation.eulerAngles.x + brX, player.rotation.eulerAngles.y + brY, player.rotation.eulerAngles.z + brZ);
			transform.position = player.transform.position + player.transform.forward * bpZ + player.right * bpX + player.up * bpY;

			timeTillNextShot -= Time.deltaTime;

//			if (Input.GetKey(KeyCode.F) && timeTillNextShot <= 0) {
//
//				newArrow = Instantiate (anArrow, arrowPosition, arrowRotation, arrowStorage);
//
//				newArrow.transform.position = arrowPosition;
//				newArrow.transform.rotation = arrowRotation;
//
//				newArrow.GetComponent<Collider> ().attachedRigidbody.useGravity = true;
//				newArrow.GetComponent<Rigidbody> ().velocity = player.transform.forward * velocity;
//
//				timeTillNextShot = NextShot;
//				Destroy (newArrow, 2.0f);
//
//			}

			if (Input.touchCount > 0 && timeTillNextShot <= 0) {


				Touch touch = Input.GetTouch (0);

				switch (touch.phase) {

				case TouchPhase.Began:
					
					newArrow = Instantiate (anArrow, arrowPosition, arrowRotation, transform);

					newArrow.transform.position = arrowPosition;
					newArrow.transform.rotation = arrowRotation;

					newArrow.GetComponent<Collider> ().attachedRigidbody.useGravity = true;
					newArrow.GetComponent<Rigidbody> ().velocity = player.transform.forward * velocity;

					timeTillNextShot = NextShot;
					Destroy (newArrow, 2.0f);

					break;
				}
			}
		}
	}

}
