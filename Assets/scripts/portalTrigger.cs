﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class portalTrigger : MonoBehaviour {


	public Material[] materials;
	public Transform mainPortal;

	bool changedWorlds;
	bool inOtherWorld;

	// Use this for initialization
	void Start () {
		
		SetMaterials (false);
	}

	void SetMaterials(bool fullRender) {


		inOtherWorld = fullRender;

		var stencilTest = fullRender ? CompareFunction.NotEqual : CompareFunction.Equal;

		foreach (var mat in materials) {

			mat.SetInt ("_StecilTest", (int)stencilTest);
		}
	}
		
	void OnTriggerEnter(Collider other) {

		if(other.transform != mainPortal) {

			return;
		}


		if (!changedWorlds) {
			
			changedWorlds = true;
			inOtherWorld = !inOtherWorld;
			SetMaterials (inOtherWorld);
		}

	}

	void OnTriggerExit (){
	
		changedWorlds = false;
	}

	void Update () {
		
	}

	void OnDestroy () {
	
		SetMaterials (true);
	}
}
