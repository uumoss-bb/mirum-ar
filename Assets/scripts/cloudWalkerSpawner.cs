﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class cloudWalkerSpawner : MonoBehaviour {


	public GameObject cloudWalker;
	public GameObject player;
	public int amount;
	int oldAmount;
	int theLevel = 1;
	public GameObject bow;


	// Use this for initialization
	void Start () {
		
		oldAmount = amount;
		bowHandler bowHandler = bow.GetComponent <bowHandler> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (bowHandler.bowInHand && amount > 0) {

			Vector3 position = new Vector3 ( Random.Range (-10f, 10f), Random.Range (1f, 5f), Random.Range (20f, 50f));

			Instantiate (cloudWalker, position, Quaternion.FromToRotation(cloudWalker.transform.forward, player.transform.forward), transform);

			amount -= 1;
		}

		if (amount == 0 && transform.childCount == 0) {
			amount = oldAmount + (theLevel + 1);
		}
	}
}
