﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class cloudWalkerAttack : MonoBehaviour {

	public GameObject player;
	public GameObject bullet;
	public GameObject bow;
	float attackSpeed; //in seconds
	bool bulletFired = true;



	// Use this for initialization
	void Start () {

		bowHandler bowHandler = bow.GetComponent <bowHandler> ();
	}

	// Update is called once per frame
	void FixedUpdate () {

		transform.LookAt (player.transform);
		attackSpeed -= Time.deltaTime;

		if (bowHandler.bowInHand && attackSpeed <= 0 && bulletFired) {
			
			attackSpeed = Random.Range (2f, 6f);
			bulletFired = false;
		}

		if (attackSpeed <= 0 && !bulletFired) {

			Vector3 position = new Vector3 (transform.position.x, transform.position.y, transform.position.z);

			GameObject newBullet = Instantiate (bullet);
			newBullet.transform.position = transform.position + transform.up + transform.forward + transform.right;
			newBullet.transform.LookAt (player.transform);
			newBullet.GetComponent<Rigidbody> ().velocity = transform.forward * 35f;

			bulletFired = true;
			Destroy (newBullet, 3.0f);
		}
	}
}
